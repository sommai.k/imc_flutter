import 'package:hooks_riverpod/hooks_riverpod.dart';

enum AuthState { auth, unAuth }

final firstNameProvider = StateProvider<String>((_) => "");
final lastNameProvider = StateProvider<String>((_) => "");
final authProvider = StateProvider<AuthState>((_) => AuthState.unAuth);

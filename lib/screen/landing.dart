import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import '../app/auth.dart';
import './login.dart';
import './full_home_page.dart';

class Landing extends HookConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final authState = ref.watch(authProvider);
    if (authState == AuthState.unAuth) {
      return Login();
    } else {
      return FullHomePage(title: "xxxx");
    }
  }
}

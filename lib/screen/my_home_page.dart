import 'package:flutter/material.dart';

class MyHomePage extends StatelessWidget {
  final String title;
  int cnt = 10;

  MyHomePage({super.key, required this.title});

  // custom function
  // when add button press
  void whenAddPress() {
    cnt = cnt + 1;
    print(cnt);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        backgroundColor: Colors.red.shade200,
        title: Text("Hello"),
      ),
      body: Center(
        child: Column(
          children: [
            Text("Number is $cnt"),
            ElevatedButton(
              onPressed: whenAddPress,
              child: Text("Add"),
            )
          ],
        ),
      ),
    );
  }
}

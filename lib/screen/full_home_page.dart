import 'package:flutter/material.dart';

class FullHomePage extends StatefulWidget {
  final String title;
  const FullHomePage({super.key, required this.title});

  @override
  State<FullHomePage> createState() => _FullHomePageState();
}

class _FullHomePageState extends State<FullHomePage> {
  int cnt = 8;
  // custom function
  // when add button press
  void whenAddPress() {
    setState(() {
      cnt = cnt + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("State Full"),
      ),
      body: Center(
        child: Column(
          children: [
            Text("$cnt"),
            ElevatedButton(
              onPressed: whenAddPress,
              child: const Text("Add"),
            )
          ],
        ),
      ),
    );
  }
}

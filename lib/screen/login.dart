import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import '../app//auth.dart';

class Login extends HookConsumerWidget {
  final _formKey = GlobalKey<FormState>();
  final userCode = TextEditingController();
  final password = TextEditingController();

  // custom function
  // call to api server
  void doLogin(WidgetRef ref) async {
    try {
      var resp = await Dio().post("http://3.0.139.90/login", data: {
        "userCode": userCode.text,
        "password": password.text,
      });
      print(resp.data);
      ref.read(firstNameProvider.notifier).state = resp.data["firstName"];
      ref.read(lastNameProvider.notifier).state = resp.data["lastName"];
      ref.read(authProvider.notifier).state = AuthState.auth;
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Column(
          children: [
            Image.asset("images/logo.png"),
            userCodeInput(
              label: "รหัสผู้ใช้งาน",
              control: userCode,
              icon: const Icon(Icons.person),
            ),
            userCodeInput(
              label: "รหัสผ่าน",
              control: password,
              icon: const Icon(Icons.lock),
              obscureText: true,
            ),
            ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  // TODO: send data to api server
                  doLogin(ref);
                }
              },
              child: const Text("เข้าสู่ระบบ"),
            )
          ],
        ),
      ),
    );
  }

  Padding userCodeInput({
    required String label,
    required Icon icon,
    required TextEditingController control,
    bool obscureText = false,
  }) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        controller: control,
        obscureText: obscureText,
        decoration: InputDecoration(
          labelText: label,
          prefixIcon: icon,
          hintText: "ระบุ$label",
          border: const OutlineInputBorder(),
          floatingLabelBehavior: FloatingLabelBehavior.always,
        ),
        validator: (value) {
          if (value!.isEmpty) {
            return "กรุณาระบุ$label";
          }
          return null;
        },
      ),
    );
  }
}

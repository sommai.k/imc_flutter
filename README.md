# imc_flutter

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

gitlab url = https://gitlab.com/sommai.k/imc_flutter

flutter --version
flutter upgrade
flutter create imc_flutter
flutter devices
flutter run -d web-server

https://gitlab.com/sommai.k/express-next

https://gitlab.com/sommai.k/npru-express

http://3.0.139.90:8081/

GET http://3.0.139.90/hi?name=xxx&msg=hello&age=18

POST http://3.0.139.90/login

flutter pub add dio

https://www.keycloak.org/
